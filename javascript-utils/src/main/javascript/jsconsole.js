
///////////////////
// A simple lucene search
var nodes = search.luceneSearch("@name:alfresco");

for each(var node in nodes) {
    logger.log(node.name + " (" + node.typeShort + "): " + node.nodeRef);
}

///////////////
// Removing aspect Site Hidden 
siteService.getSite("sto_iccs").node.removeAspect("sys:hidden")


/////////////
// Apache-commons trim in JS
var trim = Packages.org.apache.commons.lang.StringUtils.trim;
var ex = "JOE BLACK Junior";
logger.log("Fullname: "+ex);
if(ex.indexOf(' ') !== -1){
  //var fullname = ex.split("  ");
//  var cognome = fullname[0];
//  var nome = fullname[1];
  var cognome = trim(ex.substr(0,ex.indexOf(' ')));
  var nome = trim(ex.substr(ex.indexOf(' '),ex.length));
  logger.log("Nome: "+nome);
  logger.log("Cognome: "+cognome);
}


//////////////////
// Creating Content of a given Type
var node1 = userhome.createNode('test1.txt', 'cm:content');
node1.content = "Example of plain text content";
node1.save();


/////////////////
// Get Parent Folder
var doc = search.findNode("xxxx");
var parNode = doc.parents[0];

/////////////////
// Create Folder
var dest = search.findNode("destinationNoderef");
var folder = dest.createFolder("my-new-folder");


/////////////////
// Changing type (set type)
var doc = search.findNode("xxxx");
doc.specializeType("acme:duffy");
// via NodeService
var nodeServ = Packages.it.maw.choc.model.ChocModel.getServiceRegistry().getNodeService();
var contentModel = Packages.org.alfresco.model.ContentModel;
nodeServ.setType(n.nodeRef, contentModel.TYPE_CONTENT);
//var aModel = Packages.it.maw.ecm.kronosan.KronosanModel;
//nodeServ.setType(n.nodeRef, aModel.TYPE_OFFERTA_VENDITA);


//////////////////
// Delete Nodes (max results 1000, about 4/5 min elapsed)
var nodes = search.luceneSearch("+TYPE:\"acme:contact\"");
var counter=0;
for each(var node in nodes) {
  logger.log("["+counter+"] - NODE: "+node.name + " (" + node.typeShort + "): " + node.nodeRef);
  node.remove();
  counter++;
}

// Removing nodes with limit
var limit = 100;
for (var i = 0; i < limit; i++) {
	  logger.log(i+ " Elem: "+nodes[i].name);
	  nodes[i].remove();
}

//////////////////////////
// Reading from json content
// Resources: http://codebeautify.org/csv-to-xml-json#
// Source json can be a JSONArray

logger.log("Start import...");

// the json content
var node = search.findNode("workspace://SpacesStore/a113ae64-9104-4056-9405-1cc1ac44b48f"), 
  adb = search.findNode("workspace://SpacesStore/6773ab4b-127c-4d45-917a-925d477c352e"), 
  json = jsonUtils.toObject(node.content),
  trim = Packages.org.apache.commons.lang.StringUtils.trim;

for(var i in json){
  var contact = json[i], tels = [], contactNode = adb.createNode(null, "acme:contact");
  if(contact.title && (contact.title=="DIP" || contact.title=="PRF")){
      contactNode.properties["acme:personType"] = "pf";     
//      if(contact.name.indexOf('  ') !== -1){
//      	//var fullname = contact.name.split("  ");
//      	contactNode.properties["acme:surname"] = trim(contact.name.substr(0, contact.name.indexOf('  ')));
//      	contactNode.properties["acme:name"] = trim(contact.name.substr(contact.name.indexOf('  '),contact.name.length));
//      }  
  } else {
      contactNode.properties["acme:personType"] = "pg";
  }
  contactNode.properties["acme:title"] = trim(contact.title);
  contactNode.properties["acme:address"] = trim(contact.address); 
  contactNode.save();
}

/////////////////////////////
/// Get Folder by Name Path
var srcNode = search.findNode("xxxxx");
var testingFolder =srcNode.childByNamePath("QA/Performance/Testing");

/////////////////////////
/// Looping through nodes
var rootFolder = search.findNode("workspace://SpacesStore/6e9fed49-c897-4a7f-afd5-bbdc32065477");
logger.log("TOTAL childs: "+rootFolder.children.length);
//logger.log("child name: "+rootFolder.childFileFolders(true,false)[0].name);

// Less fast
for(var i=0;i<rootFolder.children.length;i++){
logger.log("child name: "+rootFolder.children[i].name);
}
// More fast
for each (el in rootFolder.children)
{
   logger.log("child name: "+el.name);
}

////////////////////////////
/// Container
siteService.getSite("sto_iccs").node;
logger.log(siteService.getSite("sto_iccs").node.nodeRef);

siteService.getSite("sto_iccs").getContainer("folder_name");
logger.log(siteService.getSite("sto_iccs").getContainer("folder_name").nodeRef);

siteService.getSite("sto_iccs").getContainer("folder_name").createFolder("Test");

// Via MDM
Packages.it.maw.choc.util.AooUtil.createChocContainer("nome-container", "site-code");

//////////////////////////
/// Removing single node
var n = search.findNode("workspace://SpacesStore/113eb8d0-b8ad-4f6d-959a-5b2781064a99");
logger.log(n.name);
n.remove();

///// CMIS
var query = "SELECT D.cmis:objectId FROM cmis:document AS D JOIN acme:readReceipt AS O ON D.cmis:objectId = O.cmis:objectId where in_tree(D,'" + container + "')";

var elements = search.query({
	language: "cmis-alfresco",
	query: query
});
	
for ( var el in elements) {
	elements[el].properties["acme:wasRead"] = true;
	elements[el].save();
}


///// Searching in folder (MAX 1000)
var base = siteService.getSite("site_name").getContainer("folder_name");
var query = "SELECT * FROM acme:protocol where in_tree('"+base+"')";

var elements = search.query({
			language: "cmis-alfresco",
			query: query
		});

logger.log("TOTAL Results: "+elements.length);
for (var el in elements) {
  logger.log("Node="+elements[el].properties["cm:name"]);
}


//////////////////// Moving Folders 
var rootScarico = search.findNode("workspace://SpacesStore/db21314b-b54a-4ff3-b0e6-0afb1de4590e");
var childs=rootScarico.getChildren();
logger.log("TOT SOURCE CHILDS:"+childs.length);
var dest = search.findNode("");
var count=0;

for each(var node in childs) {
  if(count<250){
    logger.log("");
    logger.log(node.name + " (" + node.typeShort + "): " + node.nodeRef);   
    logger.log("Orig Path: "+node.displayPath);
    node.move(dest);
    logger.log("Dest Path: "+node.displayPath);
    totalMoved = count;
  }
  count++;  
}
logger.log("");
logger.log("TOTAL MOVED: "+(totalMoved+1));


////// Massive searches and Paginated list  
/////
var base = siteService.getSite("mysite").getContainer("folder_name");
var q = "SELECT * FROM acme:protocol where in_tree('"+base.nodeRef+"')";


var maxResultsPerPage=1000;
var skipResults=0;
var page=0;
while(maxResultsPerPage==1000){
  logger.log("PAGE: "+(page+1));
  var paging = 
  { 
    maxItems: 1000, 
    skipCount: skipResults
  }; 
  logger.log("maxItems: "+maxResultsPerPage);
  logger.log("skipCount: "+skipResults);
  
  var def = 
  { 
    query: q, 
    language: "cmis-alfresco",  
    page: paging 
  };
  var currentResults = search.query(def);
  maxResultsPerPage = currentResults.length;
  //for (var el in currentResults) {
  //  logger.log("Node ID= "+currentResults[el].nodeRef);
  //  logger.log("Node Name= "+currentResults[el].properties["cm:name"]);
  //  logger.log(currentResults[el].nodeRef+"|"+currentResults[el].properties["cm:name"]+"|"+currentResults[el].properties["acme:subject"]);
  //}
  
  logger.log("Results per Page: "+maxResultsPerPage);
  skipResults += maxResultsPerPage;
  logger.log("");
  logger.log("----------------------");
  page++;
}
logger.log("");
logger.log("TOTAL RESULTS: "+skipResults);
var finish = new Date();
logger.log("Finished at: "+finish);

////////////
///////////

/////////// All Users - getPeople(filter, maxResults) get the collection of people stored in the repository.
/////////// It searches on firstName, lastName e username
var user;
var nodes = people.getPeople(null,0);
counter=0;
for each(var node in nodes)
{ 
  //logger.log(node); 
  user = utils.getNodeFromString(node); 
  logger.log(user.properties["cm:firstName"] +" " + user.properties["cm:lastName"]+ " ["+user.properties["cm:userName"]+"]"); 
  counter++;
}         
print("TOTAL:"+counter);        
////////////

///////// Get Specific User
var user = people.getPerson("robert.xxxx");
print(user.properties["cm:userName"]);
print("Pass Expiration: "+user.properties["foo:userexPassExpiration"]);


///////// Set Group Permission (Authority)
var node = search.findNode("xxxxxxx");

var authorityGroup = "GROUP_site_sitefoo_SiteConsumer"; // cm:authorityDisplayName=Utenti AOO Sitefoo

// Disable permissions inherited by parent 
node.setInheritsPermissions(false);
// Read/Write/Delete Permission
var perms = "SiteCollaborator";
//node.setPermission("SiteManager", authorityGroup);
node.setPermission(perms, authorityGroup);
	
///////// Get Permissions
var node = search.findNode("xxxxxxx");
var perms = node.getPermissions();
for(p in perms){
	logger.warn("Perm: "+perms[p]);
}

//////// Remove Permission
var node = search.findNode("xxxxxxx");
var authorityName = "GROUP_site_foogroup_uo_accesso_imprese_gdpdc";
node.removePermission("SiteCollaborator", authorityName);

///////// Check Inherit Permissions
var node = search.findNode("xxxxxxx");
logger.warn("Inherit: "+node.inheritsPermissions()); //true,false



//////// Create User
var person  = people.createPerson("test.xyz", "Test", "XYZ", "test@em.it", "12345", true);
print(person.nodeRef);

person.properties["foo:test"]=null;
person.save();

//////// Add -user to Group
var group = people.getGroup("GROUP_site_software_SiteCollaborator");
var user = people.getPerson("test.user");
people.addAuthority(group,user);

////////  Only BUILT-IN Alfresco Users (max 1000 results)
var nodes = search.luceneSearch("PATH:\"/sys:system/sys:zones/cm:AUTH.ALF/*\" AND TYPE:\"cm:person\"");
for each(var node in nodes) {
  logger.log(node.properties["cm:userName"] + " (" + node.typeShort + "): " + node.nodeRef );
 
}
logger.log("TOTAL: "+nodes.length);

////////  Only LDAP Users  (ldap1 is the name of the declared subsystem in global.properties)
var nodes = search.luceneSearch("PATH:\"/sys:system/sys:zones/cm:AUTH.EXT.ldap1/*\" AND TYPE:\"cm:person\"");
for each(var node in nodes) {
  logger.log(node.properties["cm:userName"] + " (" + node.typeShort + "): " + node.nodeRef );
}
logger.log("TOTAL: "+nodes.length);

////////  Only LDAP Users (with NO limits)
var ldapZoneNode = search.findNode("workspace://SpacesStore/c50d9614-29d3-403c-aaee-e89d837f1e20");//Company Home > System > Zones
logger.log("TOTAL: "+ldapZoneNode.children.length);

for each (u in ldapZoneNode.children){ 
  logger.log(u.properties["cm:userName"] + " (" + u.typeShort + "): " + u.nodeRef );
}


////// Import users from a TXT (avoid | separator symbol, use ; or , instead)
var node = search.findNode("workspace://SpacesStore/58a14ae5-db60-4c13-a6fd-ca92ff1dd507");
var trim = Packages.org.apache.commons.lang.StringUtils.trim;

var listaUtenti = node.content.split("\n");

logger.log("TOTAL Rows:"+listaUtenti.length);

for(var i in listaUtenti){
  
  var record = listaUtenti[i].replace("\|",";").split(";");
  
  var username=trim(record[0]);
  var nome=trim(record[1]);
  var cognome=trim(record[2]);
  var email=trim(record[3]);
  logger.log(listaUtenti[i]);
  people.createPerson(username, nome, cognome, email, "123", true);
}


//////// Only LDAP Groups (ldap-ad1 is the name of the declared subsystem in global.properties)
var nodes = search.luceneSearch("PATH:\"/sys:system/sys:zones/cm:AUTH.EXT.ldap-ad1/*\" AND TYPE:\"cm:authorityContainer\"");
for each(var node in nodes) {
  logger.log(node.properties["cm:authorityDisplayName"] + " (" + node.typeShort + ") --> " + node.nodeRef );
}
logger.log("TOTAL: "+nodes.length);

//////// All Site Groups recursively (for a given name)
var paging = utils.createPaging(-1, 0);//return all results, skip 0
var gr = groups.searchGroups("site_name-here*", paging, "fullName"); 
for (g in gr){
logger.log(g+") "+gr[g].fullName);
}

//////// All Groups across all Zones
var filter = "*";
var paging = utils.createPaging(-1, 0);// return all results, skip 0

var allGroups=  groups.getGroups(filter, paging);
for(i in allGroups ){
logger.log(i+") "+allGroups[i].fullName);
}

//////// Delete a Group
var node = people.getGroup("GROUP_my_group");
if(node){
    people.deleteGroup(node);
}   

//////// Delete a given Site and all system groups
var siteCode="kro";
var site = siteService.getSite(siteCode);

if(site){
	logger.log("Deleting Site...'"+siteCode+"'");
	site.deleteSite();
	site = siteService.getSite(siteCode);
	if(!site){
		logger.log("Site deleted, removing Site system groups...");
		var filter = "site_"+siteCode+"*";

		var paging = utils.createPaging(-1, 0);//return all results, skip 0
		var allGroups=  groups.getGroups(filter, paging);

		for(i in allGroups ){
			var groupName = allGroups[i].fullName;
			if(groupName.indexOf("site_"+siteCode)>0){
				logger.log(i+") removing..."+groupName);
              	var groupNode = people.getGroup(groupName);
				if(groupNode){
					people.deleteGroup(groupNode);
				}
			}   
		}
	}
}else{
	logger.log("Site '"+siteCode+"' not found!");
}




////////////// Creating User
/////////////
//createPerson(username, firstName, lastName, emailAddress, password, setAccountEnabled)
people.createPerson("test.test", "Test", "Svi", "test@em.it", "test", true);

//////////////Deleting User
/////////////
people.deletePerson("user-name-here");

////////////// Reset Password
///////////// 
people.setPassword("user-name-here","12345");

/////////// User Associations (source assocs)
///////////
var user;
var nodes = people.getPeople(null,0);
counter=0;
for each(var node in nodes)
{ 
	//logger.log(node); 
	user = utils.getNodeFromString(node); 
	logger.log("");
	logger.log(user.properties["cm:firstName"] +" " + user.properties["cm:lastName"]+ " ["+user.properties["cm:userName"]+"]"); 
	print("---------------------");
	var assocs = user.sourceAssocs["acme:usersAssoc"];
	if(assocs){
	 print("Total Assocs: "+assocs.length);

	 for each(aNode in assocs){
	  print("Assoc Noderef: "+aNode.nodeRef);
	  print("Assoc Name: "+aNode.name);
	  print("Assoc Site: "+aNode.siteShortName);
	  print("");
	 }

	}
	counter++;
}         
print("TOTAL USERS:"+counter);     


/////////// Associations 
///////////
var n =  search.findNode("workspace://SpacesStore/52563a60-cf3a-426f-a5da-e51f6596a4f2");

var assocs = n.sourceAssocs["acme:testAssoc"];
//var assocs =n.assocs["acme:testAssoc"];// target assocs

if(assocs){
 print("Total Assocs: "+assocs.length);

 for each(aNode in assocs){
	  print("Assoc Noderef: "+aNode.nodeRef);
	  print("Assoc Name: "+aNode.name);
	  print("");
 }

}
	
	
////////// Content Size
//////////
print(utils.getNodeFromString("workspace://SpacesStore/52563a60").getSize());

/////////// CMIS Query (NULL filtered)
///////////
var container = siteService.getSite("mysite").getContainer("folder_name");
//var q = "SELECT * FROM acme:protocol WHERE in_tree('"+container.nodeRef+"')";
var q = "SELECT * FROM acme:protocol WHERE acme:notes IS NOT NULL";
var nodes = search.query({
				language: "cmis-alfresco",
				query: q
			});
var total=0;
// Max 1000 Results
for(n in nodes){
	logger.log(nodes[n].properties["acme:protocolNumber"] +" "+nodes[n].properties["acme:notes"]);
}
logger.log("TOTAL: "+total);
///////////
///////////



/////////// TAG and TAG SCOPE
///////////
// Prendo il nodo di una email 
var n =  search.findNode("workspace://SpacesStore/52563a60-cf3a-426f-a5da-e51f6596a4f2");
print(n.nodeRef);
print(n.tagScope.tags);
n.tagScope.refresh();


/////////// TAG SCOPE 
///////////
var pecFolder = siteService.getSite("mysite").getContainer("folder_name");
print(pecFolder.name);
var count =0;
for(var i=0;i<pecFolder.children.length;i++){
  var account = pecFolder.children[i];
  if(account.isTagScope){
    print("Aggiorno child name: "+account.name);
    account.tagScope.refresh();
    count++;   
  } 
}
logger.log("");
logger.log("TOTALE TagScope aggiornati: "+count);




/////////// Delete Property
///////////
var doc = search.findNode("workspace://SpacesStore/a1fc690c-7009-4f42-b582-808cd5352ba1");
delete doc.properties["cm:lastThumbnailModification"];
doc.save();


////////////// Aspects
//////////
var n = search.findNode("workspace://SpacesStore/7ed30a76-46e5-4208-a256-4d246064f254");
logger.log(n.name);
n.addAspect('acme:trashed');
n.removeAspect('acme:trashed');
n.hasAspect('acme:trashed');
 
logger.log(n.aspectsShort); //array of short name aspects

////////////////// Datetime and ISO8601 Date
//////////////////
//Print: Tue Feb 21 2017 11:06:52 GMT+0100 (CET)
var date = new Date();
logger.log("Date: "+date);

// Print: 1487671677960
var timeInMillisecs = date.getTime();
logger.log("Millsec: "+timeInMillisecs);

// Print: 2017-02-21T11:08:55.466+01:00
var ISODate = utils.toISO8601(timeInMillisecs);
logger.log("Iso Date: "+ISODate);

// Print: Tue Feb 21 2017 11:06:52 CET 2017
var origDate = utils.fromISO8601(ISODate);
logger.log("Oringinal Date: "+origDate);

// Date TO Iso Date
var date = new Date();
var ISODate = utils.toISO8601(date);

// Date cm:created to ISO8601 Date
utils.toISO8601(node.properties["cm:created"].getTime())

// Print: 2020-03-18-175305717 (year-month-day-hour.min.sec.mill) 
var now = new Date();
var id;
id = now.getFullYear();
id += '-'+('0' + (now.getMonth()+1)).slice(-2);
id += '-'+('0' + (now.getDate())).slice(-2);
id += '-'+('0' + (now.getHours())).slice(-2);
id += ('0' + (now.getMinutes())).slice(-2);
id += ('0' + (now.getSeconds())).slice(-2);
id += ('0' + (now.getMilliseconds())).slice(-3);
logger.info("ID: "+id);

// Conversione stringa "01-MAR-13 09:17:39" in DateTime 2004-09-01T21:54:00Z
function parseDate(inputDate){
var monthStrings = [ "GEN", "FEB", "MAR", "APR", "MAG", "GIU", "LUG", "AGO", "SET", "OTT", "NOV", "DIC"];
	var dateTime = null;
	if(inputDate){
		inputDate=trim(inputDate);
		var tempDateTime = (inputDate.split(" "));
     var d = tempDateTime[0].split("-");
     for(i in monthStrings){
       if(d[1]==monthStrings[i]){
         var monthIndex=i;
       }
     }
     if(tempDateTime[1]){
	  		var t = tempDateTime[1].split(":");
	  		//JavaScript counts months from 0 to 11. January is 0. December is 11.
			dateTime = new Date("20"+d[2],monthIndex,d[0], t[0],t[1],t[2]);
	  	}else{
	  		dateTime = new Date("20"+d[2],monthIndex,d[0]);
	  	}
	}
	return dateTime;
}

// Date Parser  (from "13/04/2012")
function parseDate(inputDate){
    var dateTime = null;
    if(inputDate){
        inputDate=trim(inputDate).split("/");
        //JavaScript counts months from 0 to 11. January is 0. December is 11.
        dateTime = new Date(inputDate[2],parseInt(inputDate[1])-1,inputDate[0]);
    }
    return dateTime;
}




////////////////// Set Date on multiple nodes
/////////////////
var nodesNumb=10;
var dateTime = new Date("2010","10","30","21","30","00");
for(i=0; i < nodesNumb; i++){
  //logger.log("Element: "+i);
  var testNode = testFolder.createNode(null, "pec:mail");
  logger.log("Date: "+testNode.properties["cm:modified"]);
  testNode.addAspect("pec:trashed");
  testNode.properties["pec:mailDate"]=dateTime;
  testNode.save();
}


////////////////// Massive Query on DateTime
//////////////////
var maxDate='2016-08-25T23:59:59+02:00';
// Use Path to prevent DB query (limit 1000 results)
//var query="+PATH:\"/app:company_home/cm:test-trashed//*\" AND +TYPE:\"pec:mail\" AND +ASPECT:\"pec:trashed\" AND +pec:mailDate:[MIN TO MAX]";
var query='+PATH:"/app:company_home/cm:test-trashed//*" AND +TYPE:"pec:mail" AND +ASPECT:"pec:trashed" AND +pec:mailDate:[MIN TO \''+maxDate+'\']';

var maxResultsPerPage=1000;
var skipResults=0;
var page=0;
while(maxResultsPerPage==1000){
logger.log("PAGE: "+(page+1));
var paging = 
{ 
  maxItems: 1000, 
  skipCount: skipResults
}; 
logger.log("maxItems: "+maxResultsPerPage);
logger.log("skipCount: "+skipResults);

var def = 
{ 
  query: query, 
  language: "fts-alfresco",  
  page: paging 
};
var currentResults = search.query(def);
maxResultsPerPage = currentResults.length;
//for (var el in currentResults) {
//  logger.log("Node ID= "+currentResults[el].nodeRef);
//  logger.log("Node Name= "+currentResults[el].properties["cm:name"]);
//  logger.log(currentResults[el].nodeRef+"|"+currentResults[el].properties["cm:name"]+"|"+currentResults[el].properties["acme:subject"]);
//}

logger.log("Results per Page: "+maxResultsPerPage);
skipResults += maxResultsPerPage;
logger.log("");
logger.log("----------------------");
page++;
}
logger.log("");
logger.log("TOTAL RESULTS: "+skipResults);
var finish = new Date();
logger.log("Finished at: "+finish);



////////////////////// Classification and Categories
//////////////////////


var classificationAspects = classification.getAllClassificationAspects();
logger.log("Classification Aspects: "+classificationAspects.length);
for(var i in classificationAspects){
  logger.log(" "+classificationAspects[i]);
}
logger.log("");

var categoryRootNodesGenClass = classification.getRootCategories("cm:generalclassifiable");
logger.log("All Root Category Nodes in 'cm:generalclassifiable' :"+categoryRootNodesGenClass.length);
for(var i in categoryRootNodesGenClass){
  logger.log(" "+categoryRootNodesGenClass[i].name+" - TYPE: "+categoryRootNodesGenClass[i].type);
}
logger.log("");

var categoryNodesGenClass = classification.getAllCategoryNodes("cm:generalclassifiable");
logger.log("All Category Nodes in 'cm:generalclassifiable' : "+categoryNodesGenClass.length);
for(var i in categoryNodesGenClass){
  //logger.log(" "+categoryNodesGenClass[i].name+" - TYPE: "+categoryNodesGenClass[i].type);
}
logger.log("");


////// Removing thumbnails (type doclib and  webpreview)
//////
var nodes = search.luceneSearch("PATH:\"/app:company_home/cm:Test//*\" AND TYPE:\"cm:thumbnail\"");
for each(var node in nodes) {
  logger.log(node.name + " (" + node.typeShort + "): " + node.nodeRef );
  node.remove();
}

///// Removing node
node.remove();


////// Count nodes in Company Home by using 'recurse'
//////
var count = 0;
recurse(companyhome, function(node) {
					  if (node.isDocument) count++; 
					});
logger.log(count);


//////// Count Docs using meta.numberFound
///////
// Use Path to prevent DB query (limit 1000 results)
var q = "+PATH:'/app:company_home/st:sites/cm:cccc/cm:documentLibrary//*' AND +TYPE:'cm:content'";

var res = search.queryResultSet({
    language: "fts-alfresco",
    query: q,
    page: {
        maxItems: 10
    }
});

logger.log("TOTAL: "+res.meta.numberFound);



///////// Executing Alfresco Action (async)
////////
var action = actions.create("kro-sendmail-fornitura");

action.parameters["emailBody"] = "<h2><span style='font-size: 14px;'>Spett.le&nbsp; %RAG_SOCIALE%<br />Alla Cortese Attenzione %NOMINATIVO%</span></h2><p>Egregio Fornitore,</p><p>in vista dell'imminente applicazione del Regolamento n. 7777CE a decorrere dal 26 maggio 2021.&nbsp;</p><p>Ringraziando fin da ora per la sollecita collaborazione, restiamo in attesa di un vostro cortese riscontro.</p><p>L&rsquo;occasione &egrave; gradita per porgere i pi&ugrave; cordiali saluti.</p><p>&nbsp;</p><p>Direzione</p><p>Via Verdi, 11</p><p>2342342 Roma (Ro)</p><p>tel: +++39 12313 123132</p><p>fax: +++39 1231 2344234</p><p>web: <a href='http://www.dddd/'>http://www.dddd</a></p>"
var prot = search.findNode("workspace://SpacesStore/12684c12-eed6-49ea-ad0e-9783faf932d5");
print(prot.nodeRef);
action.parameters["noderefProt"] = prot.nodeRef;

var doc = search.findNode("workspace://SpacesStore/42017417-1d95-4a99-a4d5-2fa638bac7c7");
print(doc.nodeRef);
action.executeAsynchronously(doc);

logger.warn("ACTION Initializied...");





////////////
//////////// PURGE Site Groups
var sp = Packages.org.alfresco.util.ScriptPagingDetails();
var groupFilter = "site_rda";

//(filter,ScriptPagingDetails)
var nodes = groups.getGroups(groupFilter,sp);

for (s in nodes){
  var groupName=nodes[s].getFullName();
  print("- "+groupName); 
  if(groupName!="GROUP_"+groupFilter){
    print("Deleting secondary group....."+groupName)
    //nodes[s].deleteGroup();
  }
}

// The last to delete is primary Group
var primary = groups.getGroup("GROUP_"+groupFilter);
print("Finally deleting primary group....."+primary.getFullName());
//primary.deleteGroup();


///////////// Start/Stop SOffice

var ctxt = Packages.org.springframework.web.context.ContextLoader.getCurrentWebApplicationContext();
// Subsystem Context ( bootstrap-context, ChildApplicationContextFactory)
var subctx =  ctxt.getBean('OOoDirect', org.alfresco.repo.management.subsystems.ChildApplicationContextFactory);
// subctx.stop()
// subctx.start()



////////////// Read CSV
var csvNode = search.findNode("workspace://SpacesStore/...");

if (csvNode !== null){
    
    var csvArr = csvNode.content.split("\n");
    for(var o in csvArr){
        var rowArr = csvArr[o].split(";");
        print(rowArr);    
    }
}


////////////// Read TXT Lines
var txt = search.findNode("workspace://SpacesStore/816efb4d-d4cf-48b4-9025-8208cb8ef25b");
var trim = Packages.org.apache.commons.lang.StringUtils.trim;
  
var tot=0;
if (txt !== null){
    
  var txtLinesArr = txt.content.split("\n");
  print("Totale righe TXT: "+txtLinesArr.length.toString());  
  for(var o in txtLinesArr){
       
       var line = txtLinesArr[o].toString().trim();
      if(line){
        var nd = search.findNode(line);
        if(nd){
          if(!nd.hasAspect("itap:document")){
            print(nd.nodeRef);          
          }
          tot++;
        }        
      }
            
    }
}

print("TOTALE Nodi Trovati: "+tot);


//////////// Handling JSON Request content (webscripts)
///////////
logger.warn(requestbody.content);
var jsonObj = jsonUtils.toObject(requestbody.content);

logger.warn(JSON.stringify(jsonObj));

// Implicit JSON request body parsing: <webscript_name>.post.json.js
var jsonObj = jsonUtils.toObject(json);


///////// Bytes converter
/////////
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) return bytes + ' ' + sizes[i];
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};
// 217.9 KB
print(bytesToSize("223084"));


////////// Sort Query 
var q = "PATH:'/app:company_home/st:sites/cm:foo/cm:homes/cm:admin/cm:upload//*' AND TYPE:'cm:content' AND cm:created:2024-01-30";
var res = search.queryResultSet({
  language:"fts-alfresco",
  query:q,
  page:{maxItems:600},
  sort: [{
		column: "cm:created",
		ascending: true
	}]
});

print(res.meta.numberFound);

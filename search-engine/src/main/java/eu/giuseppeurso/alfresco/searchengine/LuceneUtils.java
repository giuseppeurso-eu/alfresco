package eu.giuseppeurso.alfresco.searchengine;


import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;


public class LuceneUtils {

	/**
	 * Alfresco 4.2.f
	 * Solr 1.4.1
	 * Lucene 2.4.1
	 *
	 * @throws IOException
	 * @throws ParseException
	 */
	public void openSolr141IndexDir() throws IOException, ParseException {

	String indexPath ="/path/to/indexes/alf_data/solr/workspace/SpacesStore/index";

	StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_24);
	File indexDir=new File(indexPath);
	Directory directory = FSDirectory.open(indexDir);

	IndexSearcher searcher = new IndexSearcher(directory);
	String queryStr = "\\{*content";
	Query query = new QueryParser(Version.LUCENE_24, "TYPE", analyzer).parse(queryStr);

	TopDocs topDocs = searcher.search(query, 10);

	ScoreDoc[] resulSet = topDocs.scoreDocs;
	for (int i = 0; i < resulSet.length; i++) {
		int docId = resulSet[i].doc;
		Document d = searcher.doc(docId);
		//System.out.println("ITEM #"+i+" : "+d.toString());
		System.out.println("ITEM #"+i);
		System.out.println("  >ID=" +d.get("ID"));
		System.out.println("  >PRIMARYPARENT=" +d.get("PRIMARYPARENT"));
		System.out.println("  >OWNER=" +d.get("OWNER"));
		System.out.println("  >TYPE=" +d.get("TYPE"));
	}

	System.out.println("Found " + resulSet.length);

}


	/**
	 * A Main to test methods
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws IOException, ParseException {
		LuceneUtils luceneUtils = new LuceneUtils();
		luceneUtils.openSolr141IndexDir();
	}

}

<html> 
    <head>
        <title>Purge Alfresco Archive</title>
    </head>
    <body>
Alfresco ${server.edition} Edition v${server.version} :
<#if elapsed??>
<p>Purged all archived nodes. Elapsed time: ${elapsed} ms.</p></body>
</#if>
</html>
